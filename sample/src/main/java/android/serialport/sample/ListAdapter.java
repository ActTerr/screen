package android.serialport.sample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListHolder> {
    Context context;
    ArrayList<String> lists;
    public ListAdapter(Context context, ArrayList<String> lists) {
        this.context = context;
        this.lists = lists;
    }
    @NonNull
    @Override
    public ListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);

        ListHolder holder = new ListHolder(view);
        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListHolder holder, int position) {
//        int index=position==0?1:position;//0时返回1，这样不会为空
        if(position!=0){
            String data=lists.get(position);
            StringTokenizer st=new StringTokenizer(data,",");
            holder.tv_order.setText(String.valueOf(position));
            holder.tv_track.setText(st.nextToken());
            int hang=Integer.parseInt(st.nextToken());
            if(hang>0){
                holder.tv_direction.setText("+");
            }else{
                holder.tv_direction.setText("-");
            }

            holder.tv_sum.setText(String.valueOf(Math.abs(hang)));
            String s=st.nextToken();
            StringTokenizer st2=new StringTokenizer(s," ");
            holder.tv_head_end.setText(st2.nextToken());
            holder.tv_car_num.setText(st2.nextToken());
            holder.tv_remarks.setText(st2.nextToken());

        }

    }

    @Override
    public int getItemCount() {
        return lists.size();
    }
    class ListHolder extends RecyclerView.ViewHolder {
        TextView tv_wocao,tv_order,tv_track,tv_direction,tv_head_end,tv_car_num,tv_remarks,tv_sum;
        ListHolder(View itemView) {
            super(itemView);

            tv_order=itemView.findViewById(R.id.tv_order);
            tv_track=itemView.findViewById(R.id.tv_track);
            tv_direction=itemView.findViewById(R.id.tv_direction);
            tv_head_end=itemView.findViewById(R.id.tv_head_end);
            tv_car_num=itemView.findViewById(R.id.tv_car_num);
            tv_remarks=itemView.findViewById(R.id.tv_remarks);
            tv_sum=itemView.findViewById(R.id.tv_sum);
        }
    }
}


