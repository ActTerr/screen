package android.serialport.sample;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.serialport.SerialPortFinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;

public class MainActivity extends SerialPortActivity {
    TextView tv_tittle,tv_notify,systemTittle,spinalling,dataA,dataB,speed;
    Button bt_return,bt_up,bt_down,bt_confirm,bt_function,bt_set,bt_help;
    RecyclerView rv;
    ListAdapter la;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initView();
        checkPort();
        String s="DD 99 00 AA 03 00 00 19 07 18 14 21 59 03 D5 BE B5 F7 00 00 01 00 01 10 00 12 30 C2 ED BD A8 BB AA 00 00 CB EF D5 BC C0 A5 00 00 00 39 05 D5 BE D5 FB 07 36 36 36 36 36 36 01 00 2A CF C2 D1 A9 C2 B7 BB AC A1 A2 D7 A2 D2 E2 B0 B2 C8 AB 0D 0A CF C2 D3 EA C2 B7 BB AC A1 A2 D7 A2 D2 E2 B0 B2 C8 AB 0D 0A 58 04 31 B5 C0 81 53 20 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 58 04 31 B5 C0 81 53 20 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 58 04 31 B5 C0 81 53 20 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 0E AE";
        s=s.replace(" ","");
        //包头2 功能码1 长度2 标示2 c1
        //     * 包头2 功能码1 长度2 标示2 调号1 命令1 时间8 语音信息20 checksum1  38
        String s1="DD 99 AA 11 11 00 01 22".replace(" ","");
        String s2="DD 99 00 AA 03 4A 00 19 4A 18 14 21 59 03 D5 BE B5 F7 00 00 01 00 01 11 22 33 11 22 33 11 22 33".replace(" ","");
        String s3="11 22 33 11 22 33".replace(" ","");
        String s5="DD 99 00 AA 03 4A 00 19 4c 18 14 21 59 03 D5 BE B5 F7 00 00 01 00 01 11 22 33 11 22 33 11 22 33".replace(" ","");
        String s6="11 22 33 11 22 33".replace(" ","");
        String s4="DD 99 aa 11 11 00 02 22".replace(" ","");
        //DD 99 00 AA 03 4A 00 19 42 18 14 21 59 03 D5 BE B5 F7 00 00 01 00 01 11 22 33 11 22 33 11 22 33 11 22 33 11 22 33
//        onDataReceived(HexUtil.decodeHex(s1),8,232);
//        onDataReceived(HexUtil.decodeHex(s2),32,232);
//        onDataReceived(HexUtil.decodeHex(s3),6,232);
//        onDataReceived(HexUtil.decodeHex(s5),32,232);
//        onDataReceived(HexUtil.decodeHex(s6),6,232);
//        onDataReceived(HexUtil.decodeHex(s4),8,232);

//        String apkRoot="chmod 777 "+getPackageCodePath();
//        RootCommand(apkRoot);


    }

    byte memory[]=new byte[1024];
    int len=0;
    boolean isStart485=false;
    boolean isStart232=false;
    @Override
    protected void onDataReceived(final byte[] buffer, final int size,final int type) {

        runOnUiThread(new Runnable() {
            public void run() {
                StringBuilder stringBuilder=new StringBuilder();
                for(int i=0;i<size;i++){
                    stringBuilder.append(buffer[i]+" ");
                }
                tv_notify.setText(stringBuilder.toString()+"type:"+232);
                if(type==485){
                if(!isStart485){
                    if(buffer[0]==-35&&buffer[1]==-103) {
                        for (int i = 0; i < size; i++) {
                            memory[len + i] = buffer[i];
                        }
                        isStart485=true;
                        len+=size;
                    }
                }else {
                    for(int i=0;i<size;i++){
                        memory[len + i] = buffer[i];
                    }
                    len+=size;

                    if (buffer[size - 2] == 0x0e && buffer[size - 1] == 0xae - 256) {
                            isStart485=false;
                            processData(memory);
                    }
                }

                }else {//232



                    if (!isStart232) {
                        if (size == 8&&buffer[6]==0x01) {
                            isStart232 = true;
                            analysisAudio(buffer);
                        }

                    } else {
                        if (size == 6||size==32) {
                            for (int i = 0; i < size; i++) {
                                memory[len + i] = buffer[i];
                            }
                            len += size;
                            if(len==38){
                                analysisAudio(memory);
                                len=0;
                            }
                        }else if(size==8&&buffer[6]==0x02){
                            analysisAudio(buffer);
                            isStart232=false;
                        }

                    }

                }

            }
        });
    }

    /**
     * 包头2 功能码1 长度2 标示2
     * 包头2 功能码1 长度2 标示2 调号1 命令1 时间8 语音信息20 checksum1
     * @param buffer
     * @param size
     */
    boolean isWrite=false;  File audioFile;
    private void analysisAudio(byte[] buffer) {
        if(!isWrite){
            if(buffer[6]==0x01) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < buffer.length; i++) {
                    sb.append(buffer[i] + " ");
                }
                Log.e("wocao", "data:" + sb.toString());
                isWrite = true;
                File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/data/");
                try {
                if (!dir.exists()) {
                    String permission = "666";//权限
                    String command = "chmod " + permission + " " + dir;
                    Runtime runtime = Runtime.getRuntime();
                    runtime.exec(command);
                }
                    File file = new File(dir, System.currentTimeMillis() + ".bin");
                    audioFile = file;
                    file.createNewFile();
                    String[] command2 = {"chmod", "777", file.getPath()};
                    ProcessBuilder builder = new ProcessBuilder(command2);
                    builder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

        }else{
            if(buffer[6]==0x02){
                isWrite=false;
            }else{
                byte number=buffer[7];
                byte command=buffer[8];
                spinalling.setText(convertCom(command));


                RandomAccessFile ras = null;
                try {
                    ras=new RandomAccessFile(audioFile,"rwd");
                    ras.seek(ras.length());
                    byte []bytes=new byte[20];
                    for(int i=0;i<20;i++){
                        bytes[i]=buffer[17+i];
                    }
                    ras.write(bytes);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    if(ras!=null){
                        try {
                            ras.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private String convertCom(byte b){
        switch (b){
            case 0x40:
                return "故障停车";
            case 0x43:
                return "紧急停车";
            case 0x4A:
                return "解锁";
            case 0x50:
                return "注意尽头线";
            case 0x5F:
                return "30米停车灯";
            case 0x51:
                return "尽头线紧急停车";
            case 0x4B:
                return "尽头线解锁";
            case 0x4D:
                return "领车";
            case 0x4E:
                return "领车完毕";
            case 0x49:
                return "十车";
            case 0x48:
                return "一车";
            case 0x42:
                return "五车";
            case 0x41:
                return "三车";
            case 0x4C:
                return "连接";
            case 0x46:
                return "溜放";
            case 0x4f:
                return "停车";
            case 0x47:
                return "推进";
            case 0x45:
                return "起动";
            case 0x44:
                return "减速";
        }
        return "";
    }

    /**
     *     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
     *
     * DD 99 00 AA 03 00 00 19 07 18 14 21 59 03 D5 BE B5 F7 00 00 01 00 01 10 00 12
     * 30 C2 ED BD A8 BB AA 00 00 CB EF D5 BC C0 A5 00 00 00 39 05 D5 BE D5 FB 07 36
     * 36 36 36 36 36 01 00 2A CF C2 D1 A9 C2 B7 BB AC A1 A2 D7 A2 D2 E2 B0 B2 C8 AB
     * 0D 0A CF C2 D3 EA C2 B7 BB AC A1 A2 D7 A2 D2 E2 B0 B2 C8 AB 0D 0A 58 04 31 B5
     * C0 81 53 20 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 58 04 31 B5 C0 81
     * 53 20 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 58 04 31 B5 C0 81 53 20
     * 31 32 33 34 35 36 37 20 D7 A2 D2 E2 B0 B2 C8 AB 0E AE
     * 帧头2数据长度2命令字1标志字节2当前时间6总勾数1机车类型6调号1单号2计划时间4编制人8调车长8
     *
     * 总长度2内容长1内容N车次长度1车次N标志位1注意事项2注意事项内容N
     * 调单号：  机车：  车次：   \n计划XX时XX分 至 XX时XX分
     */
    private void processData(byte[] buffer) {
        StringBuilder tittle=new StringBuilder();
        int len;
        int hookNum=buffer[13];//勾数
        int pos=14;
        len=6;
        String station=toString(buffer,len,pos);//站调

        int moveNum=buffer[14];//调号


        byte []bytes=new byte[2];
        bytes[0]=buffer[21]; bytes[1]=buffer[22];
        //单号
        String orderNum=HexUtil.encodeHexStr(bytes,2);
        byte times[]=new byte[8];
        times[0]=buffer[23]; times[1]=buffer[24]; times[2]=buffer[25]; times[3]=buffer[26];
        String s = HexUtil.encodeHexStr(times,4);

        String compiler=toString(buffer,6,27);//编写人

        String yardmaster=toString(buffer,6,35);//调车长
        Log.e("wocao","a"+compiler+"b"+yardmaster);
        pos=45;
        len=toInt(buffer[pos])-1; pos++;//机车长度
        Log.e("wocao","jiche:"+len);
        String train=toString(buffer,len,pos); pos+=len;//机车
        len=toInt(buffer[pos])-1; pos+=1;
        Log.e("wocao","checi:"+len);
        String trainNum=toString(buffer,len,pos); pos+=len;//车次
        Log.e("wocao","t1:"+train+" t2:"+trainNum);
        pos+=1;
        byte[] notifys = new byte[2];
        notifys[0]=buffer[pos]; notifys[1]=buffer[++pos];
        len=toInt2(notifys,2)-2;
        pos+=1;
        String notify=toString(buffer,len,pos); pos+=len;//注意事项
        //勾
        ArrayList<String> list=new ArrayList<>();
//        pos=74;
        for(int i=0;i<hookNum;i++){
           pos=analysisHook(list,buffer,pos);
        }


        tittle.append("调单号:").append(orderNum).append("  ").append("机车:").append(train).append("   ")
                .append("车次:").append(trainNum).append("    \n").append("计划").append(s.substring(0,2)).append("时")
                .append(s.substring(2,4)).append("分至").append(s.substring(4,6)).append("时").append(s.substring(6,8))
                .append("分").append("\n").append("编制人:").append(compiler).append("\n")
                .append("调车长:").append(yardmaster).append("\n");

        la=new ListAdapter(this,list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(la);
//        rv.notify();
        la.notifyDataSetChanged();
        tv_notify.setText(notify);
        tv_tittle.setText(tittle.toString());
    }

    //勾长度1股道长度1股道内容N摘挂车数1记事内容，size)
    private int analysisHook(ArrayList<String> list, byte[] buffer, int pos) {
        int result=pos+toInt(buffer[pos])-64;
        pos+=1;
        int trackLen=toInt(buffer[pos])-1;
        pos+=1;
        String track=toString(buffer,trackLen,pos);
        pos+=trackLen;
        int hangSum=buffer[pos];
        if(hangSum<0){
            hangSum+=128;
        }
        pos+=1;
        int len=result-pos;
        String remark=toString(buffer,len,pos);
        list.add(track+","+hangSum+","+remark);
        Log.e("wocao",track+","+hangSum+","+remark);
        return result;
    }


    private void checkPort() {

    }



    private int toInt(byte b){
        if(b<0){
            return b+256;
        }else{
            return b;
        }
    }

    private int toInt2(byte[] bytes,int size){
        return Integer.parseInt(new BigInteger((HexUtil.encodeHexStr(bytes,size)),16).toString(10));
    }

    private String toString(byte[] buffer,int len,int pos){
        byte bytes[]=new byte[len];
        for(int i=0;i<len;i++){
            bytes[i]=buffer[pos+i];
        }
        try {
            return new String(bytes,"GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void initView() {
        tv_tittle=findViewById(R.id.tittle);
        rv =findViewById(R.id.rv);
        tv_notify=findViewById(R.id.notify);
        systemTittle=findViewById(R.id.system_tittle);
        spinalling=findViewById(R.id.signalling);
        dataA=findViewById(R.id.dataA);
        dataB=findViewById(R.id.dataB);
        speed=findViewById(R.id.speed);
        bt_return=findViewById(R.id.bt_return);
        bt_up=findViewById(R.id.bt_up);
        bt_down=findViewById(R.id.bt_down);
        bt_confirm=findViewById(R.id.bt_confirm);
        bt_function=findViewById(R.id.bt_function);
        bt_set=findViewById(R.id.bt_set);
        bt_help=findViewById(R.id.bt_help);
    }
    public static boolean RootCommand(String command)
    {
        Process process = null;
        DataOutputStream os = null;
        try
        {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(command + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e)
        {
            Log.d("*** DEBUG ***", "ROOT REE" + e.getMessage());
            return false;
        } finally
        {
            try
            {
                if (os != null)
                {
                    os.close();
                }
                process.destroy();
            } catch (Exception e)
            {
            }
        }
        Log.d("*** DEBUG ***", "Root SUC ");
        return true;
    }


}
